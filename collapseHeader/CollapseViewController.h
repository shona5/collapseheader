//
//  CollapseViewController.h
//  collapseHeader
//
//  Created by Don Asok on 06/12/16.
//  Copyright © 2016 Don Asok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollapseViewController : UIViewController


@property(nonatomic, weak) IBOutlet NSLayoutConstraint *contentViewHeight;


/**
 @property headerImageView
 @description showing header image view for the post
 */
@property(nonatomic, weak) IBOutlet UIImageView *headerImageView;

/**
 @property contentView
 @description view where we add our other controls
 */
@property(nonatomic, weak)  IBOutlet UIView *contentView;

/**
 @property headerImageViewHeight
 @description value for setting header image height
 */
@property(nonatomic, weak)  IBOutlet NSLayoutConstraint *headerImageViewHeight; //default half of screen size


/**
 @method adjustContentViewHeight
 @description this will adjust content view height
 */
-(void)adjustContentViewHeight;



@end
