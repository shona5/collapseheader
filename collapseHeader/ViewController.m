//
//  ViewController.m
//  collapseHeader
//
//  Created by Don Asok on 06/12/16.
//  Copyright © 2016 Don Asok. All rights reserved.
//

#import "ViewController.h"
#import "CollapseViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.headerImageViewHeight.constant = 250;
    [self adjustContentViewHeight];
    self.contentViewHeight.constant = [UIScreen mainScreen].bounds.size.height;
    
    UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(0, 10, 180, 21)];
    lable.text = @"Parallax View";
    
    [self.contentView addSubview:lable];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
