//
//  CollapseViewController.m
//  collapseHeader
//
//  Created by Don Asok on 06/12/16.
//  Copyright © 2016 Don Asok. All rights reserved.
//

#import "CollapseViewController.h"

@interface CollapseViewController ()<UIScrollViewDelegate>

@property(nonatomic, weak) IBOutlet UIScrollView *bottomScroll;

/**
 @property topScroll
 @description UIScrollView place at top of View holding post image
 */
@property(nonatomic, weak) IBOutlet UIScrollView *topScroll;
/**
 @property scrollDirectionValue
 @description holding value to determine scroll direction
 */
@property(nonatomic, assign) float scrollDirectionValue;

/**
 @property yoffset
 @description set scroll contentoffset based on this offest value
 */
@property(nonatomic, assign) float yoffset;


/**
 @property alphaValue
 @description alpha to  fade in fade out nav color
 */
@property(nonatomic, assign) CGFloat alphaValue;
/**
 @property bottomViewTopConstraint
 @description constraint for aligning bottom view as per our post imageview height
 */
@property(nonatomic, weak) IBOutlet NSLayoutConstraint *bottomViewTopConstraint;




@end

@implementation CollapseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIView *view = [[[NSBundle mainBundle]loadNibNamed:@"CollapseViewController" owner:self options:nil] objectAtIndex:0];
    view.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    [self.view insertSubview:view atIndex:0];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    self.bottomScroll.delegate = self;
    //self.headerImageViewHeight.constant = HEADER_IMAGE_HEIGHT;
    self.bottomViewTopConstraint.constant = self.headerImageViewHeight.constant;
    //self.contentViewHeight.constant = [UIScreen mainScreen].bounds.size.height - HEADER_IMAGE_HEIGHT;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)adjustContentViewHeight{
    self.bottomViewTopConstraint.constant = self.headerImageViewHeight.constant;
    self.contentViewHeight.constant = [UIScreen mainScreen].bounds.size.height -  self.headerImageViewHeight.constant;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
